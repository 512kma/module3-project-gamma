## Journal Entries:

################################## WK 1 ##################################

3/20/23 - 3/23/23:
Introduction to FastAPI and assigned teams. Met with team and figured out what app we want to build. Built out wireframes for app called 'ShapeShifters' and started working on building our API routes (just the design, not actual code). Learned how to interact with PostgreSQL and use FastAPIs - which appears to be easier to use that the RESTful approach.

3/24/23:
Finishing up API design and plan to have Candice review it. A lecture about MongoDB from Jimmy and Riley. I'm 100% planning on reviewing all videos, lectures, and modules to review PostgreSQL and FastAPI, as well as, tutorials on MongoDB - that way on Monday, our group can hit the ground running. Team decided that will be using PostgreSQL.

################################## WK 2 ##################################
3/27/23:
Went to the dentist so I got a little bit of a late start. Set up tables and did init migrations.

3/28/23:
started off the day with some SQL practice problems, some good lectures, and some career stuff. Set up tables and did init migrations.
We got authentication working finally, and tomorrow awe are goign to start building our main routes.

3/29/23:
Building routes, running into bugs but are making progress...

3/30/23
Building routes, running into bugs but are making progress...
WE called it an early day so that we could enjoy the long weekend.

################################## WK 3 ##################################

4/3/23
All the events routes/queries are almost complete, we are running into issues with updating an event.

4/4/23
completed almost all of the back-end: events and attendees commpleted. We also protected some routes with jwtdown for fastapi. Overall, pretty successfull day.

4/5/23

4/6/23

4/7/23

################################## SPRING BREAK|WK 4 ##################################

################################## WK 5 ##################################

4/17/23
completed the login/logout/signup, all around front-end authentication, with the front end over spring break. spent some time reviewing code with teams members. created new branches and issues. strated building individual components for the application. we ended the day discussing our next steps and breaking into groups so we can divide and conquer the remaining tasks.

4/18/23 - 4/20/23
wife was admitted to hospital and my daughter was born.

4/24/23
took practice exam then went back to hospital. my group has been filling me in on code/changes they've done. pulled code and reviewing so I can catch up.

4/25/23
started day in afternoon after visit to hospital. contuniation of reviewing code and strating to plan a test to implement.

4/26/23
started the day close to noon. the group was working updating an event and after discussing with an instructor, we decided to make that functionality a stretch goal. we cleaned up code and removed 'print' and 'console.log' statements. we linked git issues to corresponding merge requests. started to build the footer - which is also a stretch goal.

4/27/23
continuing to work on the footer, tyler is helping us with this experties with tailwind.

4/28/23
changed code a little and made it so the user return to the landing page when they logout. little refactoring and cleaning code.
